﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using ReportServer.Models;
using System;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using WebGrease.Css.Extensions;

namespace ReportServer.Controllers
{
  public class ReportsController : Controller
  {
    private static readonly string ConStr = ConfigurationManager
      .ConnectionStrings["SirekasConnectionString"]
      .ConnectionString;

    private readonly SqlConnectionStringBuilder _builder = new SqlConnectionStringBuilder(ConStr);

    [HttpGet]
    public ActionResult Test()
    {
      var tahun = Request.Headers["x-api-tahun"];

      return new HttpStatusCodeResult(HttpStatusCode.OK, $"Report Server Running! Years: {tahun}");
    }

    [HttpPost]
    public ActionResult GetReport(ParamReport paramReport)
    {
      try
      {
        var tahun = Request.Headers["x-api-tahun"];

        if (string.IsNullOrWhiteSpace(tahun))
          throw new ArgumentNullException(nameof(tahun), "Header x-api-tahun cannot be null");

        int numTahun;
        var isTahunNum = int.TryParse(tahun, out numTahun);

        if (!isTahunNum)
          throw new ArgumentException("Header x-api-tahun not in correct format", nameof(tahun));

        if (!ModelState.IsValid) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

        var dbName = _builder.InitialCatalog.Split('_')[0];

        if (string.IsNullOrWhiteSpace(dbName))
          throw new ArgumentNullException(nameof(dbName), "Database name cannot be null");

        var conInfo = new ConnectionInfo
        {
          ServerName = _builder.DataSource,
          DatabaseName = string.Concat(dbName, "_", tahun),
          UserID = _builder.UserID,
          Password = _builder.Password
        };

        var rd = new ReportDocument();

        rd.Load(Path.Combine(Server.MapPath("~/Reports"), paramReport.ReportName));
        rd.DataSourceConnections[0]
          .SetConnection(conInfo.ServerName, conInfo.DatabaseName, conInfo.UserID, conInfo.Password);

        if (paramReport.Parameters.Any())
        {
          paramReport.Parameters.ForEach(p => { rd.SetParameterValue(p.Key, p.Value); });
        }

        Response.Buffer = false;

        Response.ClearContent();

        Response.ClearHeaders();

        ExportFormatType streamFormat;

        string contentType;

        switch (paramReport.FormatType)
        {
          case ReportType.Pdf:
            streamFormat = ExportFormatType.PortableDocFormat;
            contentType = "application/pdf";
            break;
          case ReportType.Word:
            streamFormat = ExportFormatType.WordForWindows;
            contentType = "application/msword";
            break;
          case ReportType.Excel:
            streamFormat = ExportFormatType.Excel;
            contentType = "application/vnd.ms-excel";
            break;
          default:
            streamFormat = ExportFormatType.PortableDocFormat;
            contentType = "application/pdf";
            break;
        }

        var stream = rd.ExportToStream(streamFormat);

        stream.Seek(0, SeekOrigin.Begin);

        rd.Dispose();

        return File(stream, contentType);
      }
      catch (Exception ex)
      {
        return new JsonResult
        {
          Data = new
          {
            statusCode = HttpStatusCode.BadRequest,
            errorMessage = ex.InnerException?.Message ?? ex.Message
          }
        };
      }
    }
  }
}
